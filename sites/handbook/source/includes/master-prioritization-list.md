| Priority | Description | Issue label(s) |
| ------ | ------ | ------ |
| 1* | <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security fixes</a> | `security` |
| 2 | Data-loss prevention | `data loss` | 
| 3* | <a href="/handbook/engineering/performance/index.html#availability">Availability</a>, <a href="/handbook/engineering/workflow/#infradev">Infradev</a>, Incident Corrective Actions | `availability`, `infradev`, `Corrective Action`  | 
| 4 | Fixing regressions (things that worked before) | `regression` |
| 5 | Performance Refinement | `performance-refinement` |
| 6 | Promised to Customers | `planning-priority`, `customer`, `customer+` |
| 7 | Instrumentation improvements, particularly for xMAU | `instrumentation` |
| 8 | Usability Improvements and User Experience to drive xMAU |`feature::enhancement`, `UX debt`|
| 9 | IACV Drivers | |
| 10 | Identified for Dogfooding | `Dogfooding::Build in GitLab`, `Dogfooding::Rebuild in GitLab` |
| 11 | Velocity of new features, technical debt, community contributions, and all other improvements | `direction`, `feature`, `technical debt` |
| 12 | Behaviors that yield higher predictability (because this inevitably slows us down) | `predictability` |

*indicates forced prioritization items with SLAs/SLOs

### Engineering Allocation

Engineering is the DRI for mid/long term team efficiency, performance, security (incident response and anti-abuse capabilities), availability, and scalability. The expertise to proactive identify and iterate on these are squarely in the Engineering team. Whereas Product can support in performance issues as identified from customers. In some ways these efforts can be viewed as risk-mitigation or revenue protection. They also have the characteristic of being larger than one group at the stage level. Development would like to conduct an experiment to focus on initiatives that should help the organization scale appropriately in the long term.  We are treating these as a percent investment of time associated with a stage or category. The percent of investment time can be viewed as a prioritization budget outside normal Product/Development assignments.  

Engineering Allocation is also used in short-term situations in conjunction and in support of maintaining acceptable Error Budgets for GitLab.com and our [GitLab-hosted first](/direction/#gitlab-hosted-first) theme.

Unless it is listed in this table, Engineering Allocation for a stage/group is 0% and we are following normal [prioritization](/handbook/product/product-processes/#prioritization). Refer to this [page](https://about.gitlab.com/handbook/engineering/engineering-allocation/) for Engineering Allocation charting efforts. Some stage/groups may be allocated at a high percentage or 100%, typically indicating a situation where all available effort is to be focused on Reliability related (top 5 priorities from [prioritization table](/handbook/product/product-processes/#prioritization)) work.

Mid/long term initiatives are engineering-led. The EM is responsible for recognizing the problem, creating a satisfactory goal with clear success criteria, developing a plan, executing on a plan and reporting status.  It is recommended that the EM collaborate with PMs in all phases of this effort as we want PMs to feel ownership for these challenges.  This could include considering adding more/less allocation, setting the goals to be more aspirational, reviewing metrics/results, etc.   We welcome strong partnerships in this area because we are one team even when allocations are needed for long-range activities.

| Group/Stage | Description of Goal | Justification | Maximum % of headcount budget | Supporting information | EMs / DRI | PMs | 
| ------ | ------ | ------- | ------ | ------ | ------- |  ------ | 
| Gitaly | Improve sync between Gitaly Cluster and Praefect Database to remove out of sync conditions | [Remove split-brain conditions](https://gitlab.com/gitlab-org/gitlab/-/issues/336539) impacting large deployments |  100% | Additional Epic(s) TBD / [Epic](https://gitlab.com/groups/gitlab-org/-/epics/5717) <a href="/direction/create/gitaly_authx/index.html">Directions Page</a> | @pks-t | @mjwood |
| Manage:Access | Improve performance of AuthorizedProjectsWorker | Following work to improving performance for Namespaces |  30% | Additional Epic(s) TBD / [Improve performance of AuthorizedProjectsWorker](https://gitlab.com/groups/gitlab-org/-/epics/3343) | @lmcandrew | @ogolowinski  |
| Manage:Access | Security burn-down | More security issues exist in the backlog and are incoming than the team is able to prioritize |  43% | Additional Epic(s) TBD | @lmcandrew | @ogolowinski  |
| Manage:Access | Scalability of AuthN/AuthZ functionality (Workspace) | Reduce duplication of code and increase performance for Groups/Projects |  14% | Additional Epic(s) TBD | @lmcandrew | @ogolowinski  |
| Create:Code Review | Improving Error Budget and working Infra-Dev Issues |  | 100% |  |  |    | 
| Verify:Testing | Improving Error Budget and working Infra-Dev Issues | Allocation to Verify:Pipeline Execution Infradev Issues | 50% | [Testing Investigation Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/331163)  | @jheimbuck_gl  |  @shampton   | 
| Manage:Import | Improving Error Budget, Infra-Dev Issues, and Security |  | 50% |  |  |    | 
| Create:Source Code | Improving Error Budget and working Infra-Dev Issues |  | 100% |  |  |    | 
| Verify:Pipeline Execution | Improving Error Budget and working Infra-Dev Issues | Large backlog of Infra-Dev issues | 100% | [CI Category Direction](https://about.gitlab.com/direction/verify/continuous_integration/#whats-next--why) | @jreporter |  @jreporter  | 
| Verify | Scale GitLab.com to 20M builds a day | Give us 2 years of runway | 10% | [CI Scaling Target](https://gitlab.com/groups/gitlab-org/-/epics/5745) | @darbyfrey | @jreporter |
| Verify:Runner | Improve Runner deployment to GitLab.com Shared Runners. Target reduction in time to deploy of 80%. | Increase operational responsiveness and team efficiency/standardization |  30% | [CD for Runner](https://gitlab.com/groups/gitlab-org/-/epics/1453) | @erushton | @DarrenEastman | 
| Database | Primary Key overflow, Retention Strategy, Schema Validation, migration improvements, testing | Database has been under heavy operational load and needs improvement | 100% | [Automated Migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6), [Automated migrations for primary key conversions](https://gitlab.com/groups/gitlab-org/-/epics/5654), [Remove PK overflow](https://gitlab.com/groups/gitlab-org/-/epics/4785), [Schema Validation](https://gitlab.com/groups/gitlab-org/-/epics/3928), [Reduce Total size of DB](https://gitlab.com/groups/gitlab-org/-/epics/4181) | @craig-gomes | TBD |
| Fulfillment | [Improve availability of CustomersDot by migrating from Azure to GCP | Improve availability of CustomersDot due to [several recent outages](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Service%3A%3ACustomers) | 15% (3 Fulfillment Engineers + 0.67 Infrastructure Engineers) | [Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/390) | @jeromezng | @justinfarris |

#### Broadcasting and communication of Engineering Allocation direction 

Each allocation has a [direction page](/handbook/product/product-processes/#managing-your-product-direction) maintained by the Engineering Manager. The Engineering Manager will provide regular updates to the direction page. Steps to add a direction page are:

1. Open an MR to the [direction content](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/)
1. Add a directory under the correct stage named for the title Engineering Allocation 
1. Add a file for the page named `index.html.md` in the newly created directory 

To see an example for an Engineering Allocation Direction page, see [Continuous Integration Scaling](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/direction/verify/continuous_integration_scaling/index.html.md). Once the Engineering Allocation is complete, delete the direction page. 

#### How to get a effort added to Engineering Allocation

One of the most frequent questions we get as part of this experiment is "How does a problem get put on the Engineering Allocation list?".  The short answer is someone makes a suggestion and we add it.  Much like everyone can contribute, we would like the feedback loop for improvement and long terms goals to be robust.  So everyone should feel the empowerment to suggest an item at any time.  

To help with getting items that on the list for consideration, we will be performing a survey periodically.  The survey will consist of the following questions:

1. If you were given a % of engineering development per release to work on something, what would it be?
1. How would you justify it?

We will keep the list of questions short to solicit the most input.  The survey will go out to members of the Development, Quality, Security.  After we get the results, we will consider items for potential adding as an Engineering Allocation.  
